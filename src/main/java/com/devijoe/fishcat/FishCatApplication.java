package com.devijoe.fishcat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FishCatApplication {
    public static void main(String[] args) {
        SpringApplication.run(FishCatApplication.class, args);
    }
}
